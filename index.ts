import {
  Client,
  GuildMember,
  Formatters,
  Intents,
  MessageEmbed,
} from "discord.js";
import { config } from "dotenv";

config();

const client: Client = new Client({
  intents: [
    Intents.FLAGS.GUILDS,
    Intents.FLAGS.GUILD_MESSAGES,
    Intents.FLAGS.GUILD_PRESENCES,
    Intents.FLAGS.GUILD_MEMBERS,
  ],
});

client.on("ready", () => {
  if (client === undefined) return;
  console.log(`Bot logged in as ${client.user?.tag}`);
  client.user?.setActivity(`the game of life`);
});

// this is where the magic happens
client.on("presenceUpdate", async (originalPresence, newPresence) => {
  if (!originalPresence) return;

  if (originalPresence.status !== newPresence.status) {
    const guild = originalPresence.guild;
    await guild?.members.fetch();

    const member = originalPresence.member;

    if (!process.env.AUTHOR_ID) return;

    const author: GuildMember | undefined = guild?.members.cache.get(
      process.env.AUTHOR_ID
    );

    const dm = await author?.createDM();

    if (!member) return;

    const PRESENCE_COLORS = {
      dnd: 15158332,
      idle: 16776960,
      offline: 9807270,
      online: 3066993,
      invisible: 9807270,
    };

    const embed = new MessageEmbed({
      title: "Presence Change",
      description: `Presence change detected for <@${member.id}>.`,
      color: PRESENCE_COLORS[newPresence.status],
      thumbnail: {
        url: member.displayAvatarURL({ format: "jpg", size: 512 }),
      },
      fields: [
        { name: "User:", value: `${Formatters.italic(member?.user.tag)}` },
        { name: "User ID:", value: `${Formatters.italic(member?.user.id)}` },
        {
          name: "Old Presence:",
          value: `${Formatters.inlineCode(originalPresence.status)}`,
          inline: true,
        },
        {
          name: "New Presence:",
          value: `${Formatters.inlineCode(newPresence.status)}`,
          inline: true,
        },
        { name: "TimeStamp:", value: `${Formatters.time()}` },
      ],
    });

    dm?.send({ embeds: [embed] });
  }
});

client.login(process.env.TOKEN);
